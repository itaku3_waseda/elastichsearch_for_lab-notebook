# 検索エンジン(word2vec & elasticsearch) 
  
***環境構築***  
*1. 実行に必要な環境*

・Docker  
・Docker-Composer  
・Python 3以降
  
*2. 環境構築作業*
```
※Elastichsearch_for_lab-notebook/で以下のコマンドを入力する

$ docker-compose build			#dockerで環境を構築
$ docker-compose up	-d			#Elasticsearch, Elasticsearch-headを起動
$ docker-compose down			#Elasticsearch, Elasticsearch-headを停止
```

起動が完了すると以下のURL(ポート番号)にそれぞれ用意されます  

- elasticsearch  
>http://localhost:9200/
  
- elasticsearch-head(ここからESを確認/操作できる)  
>http://localhost:9100/  
---



***実行方法***  
*1. Excelファイルのデータを読み込み，ESに登録，検索する*
```
※ Elastichsearch_for_lab-notebook/で以下のコマンドを入力する

$ docker-compose up	-d			#すでに起動済みの場合は不要
$ python ./src/main_excel.py <COMMAND>
```
<COMMAND\>に入る文字列は以下の３種類
```
 "regist" : create DB's index
 "delete" : delete DB's index 	#DBにデータがない状態で"delete"を実行するとERRORが出ます．
 "search" : search DB's index
```  

この時，仕様しているデータはそれぞれ以下にあります  

 - 検索対象のExcelデータ
> ./data/excel_input.exe

 - 検索条件
> ./data/search_info(exel).txt

*1. Wordファイルのデータを読み込み，ESに登録，全文検索する*
```
※ Elastichsearch_for_lab-notebook/で以下のコマンドを入力する

$ docker-compose up	-d			#すでに起動済みの場合は不要
$ python ./src/main_word.py
```
その後COMMANDを入力．
```
 "regist" : create DB's index
 "delete" : delete DB's index
 "search" : search DB's index
```
[!]DBにデータがない状態で"delete"を実行するとERRORが出ます．  

この時，仕様しているデータはそれぞれ以下にあります  

- 検索対象のWordデータ
> ./data/word_input.docx

- DB(ES)の分析設定データ
> ./data/analyze_info(word).txt

- 検索条件
> ./data/search_info(word).txt