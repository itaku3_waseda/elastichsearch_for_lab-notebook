# plugin
from elasticsearch import Elasticsearch
import time
import json
import pprint

# input
from input import input_excel
from input import input_word
from input import input_txt
# function
from function import func_elasticsearch

database = Elasticsearch("localhost:9200")

#--------------------------------------------
# define
index_name = "excel_document"
type_name = "n/a"
database_url = "localhost:9200"
delay = 5

input_address_exel = "./src/data/excel_input.xlsx"
input_address_search_info = "./src/data/search_info(excel).txt"


def regist():
	input_data = input_excel.input(input_address_exel)
	pprint.pprint(input_data, indent=2)
	for i in input_data:
		database.index(index=index_name, doc_type=type_name, body=input_data[i])
	print("----------------------------------------------------")
	print("[!] Registration is COMPLETE🍺")

def delete():
	database.indices.delete(index=index_name)
	print("[!] Delete is COMPLETE🍺")

def search():
	search_info = input_txt.input(input_address_search_info)
	res = database.search(index=index_name, body=search_info)
	# pprint.pprint(res["hits"]["hits"],indent=2)
	print(json.dumps(res["hits"]["hits"], ensure_ascii=False, indent=2))
	print("----------------------------------------------------")
	print("[!] Search is COMPLETE🍺")


# 直接実行された時
if __name__ == '__main__':#直接実行した時だけ、def main()を実行する
	import sys
	args = sys.argv

	database = Elasticsearch(database_url)

	if(len(args)!=2):
		print("====================================================")
		print("Please write <COMMAND>!")
		print("execute command is \"python main_excel.py <COMMAND>\"")
		print("----------------------------------------------------")
		print("COMMAND List")
		print(">\"regist\" : create DB's index")
		print(">\"delete\" : delete DB's index")
		print(">\"search\" : search DB's index")
		print("====================================================")
		print()
	else:
		if(args[1] == "regist"):
			regist()
		elif(args[1] == "delete"):
			delete()
		elif(args[1] == "search"):
			search()
		else:
			print("====================================================")
			print("Please write <COMMAND>!")
			print("execute command is \"python main_excel.py <COMMAND>\"")
			print("----------------------------------------------------")
			print("COMMAND List")
			print(">\"regist\" : create DB's index")
			print(">\"delete\" : delete DB's index")
			print(">\"search\" : search DB's index")
			print("====================================================")
			print()

