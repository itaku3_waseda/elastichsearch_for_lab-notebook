# plugin
import pandas as pd

# -----------------------------------------------------------------------------------
# "../main"で呼ばれる関数
def input(file_name):
	df = pd.read_excel(file_name)
	# print(df)
	
	dict_df = df.to_dict(orient='index')

	# print(dict_df)

	return dict_df

# -----------------------------------------------------------------------------------
# テスト用
if __name__ == '__main__':#直接yobareru.pyを実行した時だけ、def test()を実行する
    input("../data/fruit.xlsx")