# plugin


# -----------------------------------------------------------------------------------
# "../main"で呼ばれる関数
def input(file_name):
	file = open(file_name)
	file_data = file.read()  # ファイル終端まで全て読んだデータを返す
	file.close()

	return file_data

# -----------------------------------------------------------------------------------
# テスト用
if __name__ == '__main__':#直接yobareru.pyを実行した時だけ、def test()を実行する
    input("../data/search_info.xlsx")