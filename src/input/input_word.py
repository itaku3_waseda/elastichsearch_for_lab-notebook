# plugin
import docx

def input(file_name):
	doc = docx.Document(file_name)
	txt = []
	
	for par in doc.paragraphs:
		txt.append(par.text)
		
	# for i in txt:
	# 	print(i)

	return txt

# -----------------------------------------------------------------------------------
# テスト用
if __name__ == '__main__':#直接yobareru.pyを実行した時だけ、def test()を実行する
    input("../data/shigenobu.docx")
