# plugin
from elasticsearch import Elasticsearch
import json
import time

delay = 1
data = Elasticsearch("localhost:9200")

# -----------------------------------------------------------------------------------
# Elasticsearchの挙動確認
def elasticsearch():
	data.indices.delete(index="fruit")

	# インデックスの設定
	data.index(index="fruit", doc_type="test", id=1, body={"name":"apple", "color":"red"})

	# インデックスの要素の追加
	data.index(index="fruit", doc_type="test", id=2, body={"name":"banana", "color":"yellow"})
	data.index(index="fruit", doc_type="test", id=3, body={"name":"strawberry", "color":"red"})
	data.index(index="fruit", doc_type="test", id=4, body={"name":"cherry", "color":"red"})
	data.index(index="fruit", doc_type="test", id=5, body={"name":"pomegranate", "color":"red"})

	# インデックスの要素の変更(上書き)
	# es.index(index="fruit", doc_type="test", id=1, body={"name":"apple", "color":"green"})

	# 全部持ってくる
	# res = es.search(index="fruit", body={"query": {"match_all": {}}})

	# 全文探索 (resは辞書型)
	time.sleep(delay)
	res = data.search(index="fruit", body={"query": {"match": {"color":"red"}}})
	print(json.dumps(res["hits"]["hits"], indent=2))

	# データの一個目を出力
	# print(json.dumps(res["hits"]["hits"][0], indent=2))


# -----------------------------------------------------------------------------------
# "../main"で呼ばれる関数
def search(database_url, es_index, es_body):
	database = Elasticsearch(database_url)
	return database.search(index=es_index, body=es_body)

def analyze(database_url, es_index, es_body):
	database = Elasticsearch(database_url)
	return database.indices.analyze(index=es_index, body=es_body)

def dict_registration(database_url, es_index, es_type, es_dict):
	database = Elasticsearch(database_url)
	database.index(index=es_index, doc_type=es_type, body=es_dict)

def registration(database_url, es_index, es_type, es_script):
	database = Elasticsearch(database_url)
	database.index(index=es_index, doc_type=es_type, body={"script":es_script})

def update(database_url, es_index, es_type, es_id, es_name, es_color):
	database = Elasticsearch(database_url)
	database.index(index=es_index, doc_type=es_type, id=es_id, body={"name":es_name, "color":es_color})

def id_delete(database_url, es_index, es_type, es_id):
	database = Elasticsearch(database_url)
	database.delete(index=es_index, doc_type=es_type, id=es_id)

def query_delete(database_url, es_index, es_type, query_name, query_content):
	database = Elasticsearch(database_url)
	database.delete_by_query(index=es_index, doc_type=es_type, body={'query': {'match': {query_name: query_content}}})

def index_delete(database_url, es_index):
	database = Elasticsearch(database_url)
	database.indices.delete(index=es_index)


# -----------------------------------------------------------------------------------
# テスト用
if __name__ == '__main__':#直接yobareru.pyを実行した時だけ、def test()を実行する
    elasticsearch()

