# plugin
from elasticsearch import Elasticsearch
import time
import json
import sys
import pprint

# input
from input import input_excel
from input import input_word
from input import input_txt
# function
from function import func_elasticsearch

database = Elasticsearch("localhost:9200")

#--------------------------------------------

def regist():
	analyze_info = input_txt.input(input_address_analyze_info)
	database.indices.create(index=index_name, body=analyze_info)
	# pprint.pprint(analyze_info, indent=2)
	print("[!] Registration is COMPLETE🍺")
	print()
	print()

def add(file_name):
	input_address_word = "./src/data/"
	input_address_word = input_address_word + file_name
	input_data = input_word.input(input_address_word)
	print(json.dumps(input_data, ensure_ascii=False, indent=2))
	for data in input_data:
		database.index(index=index_name, doc_type=file_name, body={"script":data})
	print("[!] Registration is COMPLETE🍺")
	print()
	print()

def delete():
	database.indices.delete(index=index_name)
	print("[!] Delete is COMPLETE🍺")
	print()
	print()

def search(word):
	# search_info = input_txt.input(input_address_search_info)
	res = database.search(index=index_name, body={"query": {"match": {"script":word}}})
	# pprint.pprint(res["hits"]["hits"],indent=2)
	print(json.dumps(res["hits"]["hits"], ensure_ascii=False, indent=2))
	print("-----------------------------------------------")
	print("File List [search word \"" + word + "\"]")
	for match_data in res["hits"]["hits"]:
		print(">", end="")
		print(match_data["_type"])

	print("-----------------------------------------------")
	print("[!] Search is COMPLETE🍺")
	print()
	print()


# 直接実行された時
if __name__ == '__main__':#直接実行した時だけ、def main()を実行する
	# import sys
	# args = sys.argv
	index_name = "files"
	type_name = "n/a"
	database_url = "localhost:9200"

	input_address_word = "./src/data/"
	input_address_analyze_info = "./src/data/analyze_info(word).txt"
	# input_address_search_info = "./src/data/search_info(word).txt"

	database = Elasticsearch(database_url)
	print()
	print("====================================================")
	print("Please type COMMAND.")
	print("----------------------------------------------------")
	print("COMMAND List")
	print(">\"regist\" : create DB's index")
	print(">\"delete\" : delete DB's index")
	print(">\"add\"    : add new file to DB")
	print(">\"search\" : search DB's index")
	print(">\"down\"   : down this App")
	print("====================================================")
	print()

	while 1:
		print("COMMAND : ", end="")
		command = input()
		print("----------------------------------------------------")

		if(command == "add"):
			print("Add File Name : ", end="")
			file_name = input()
			print("----------------------------------------------------")
			add(file_name)
		if(command == "regist"):
			regist()
		elif(command == "delete"):
			# print("Delete File Name : ", end="")
			# file_name = input()
			# print("----------------------------------------------------")
			# delete(file_name)
			delete()
		elif(command == "search"):
			print("Search Word : ", end="")
			word = input()
			print("----------------------------------------------------")
			search(word)
		elif(command == "down"):
			print("DONE APPLICATION DOWN!")
			print("GOOD LUCK🍺")
			print()
			break
		else:
			print("====================================================")
			print("Please type CORRECT command.")
			print("----------------------------------------------------")
			print("COMMAND List")
			print(">\"regist\" : create DB's index")
			print(">\"delete\" : delete DB's index")
			print(">\"search\" : search DB's index")
			print(">\"down\"   : down this App")
			print("====================================================")
			print()
